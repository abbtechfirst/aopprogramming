package org.example.aopspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(AopSpringApplication.class, args);
        TestClass test = new TestClass();
        test.securedMethod(Role.ADMIN);
        test.securedMethod(Role.USER);
        test.unsecuredMethod();
    }

}
