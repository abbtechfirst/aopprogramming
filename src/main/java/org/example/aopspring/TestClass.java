package org.example.aopspring;

public class TestClass {

    @Secured(Role.ADMIN)
    public void securedMethod(Role role) {
        System.out.println("Inside securedMethod");
    }

    @Loggable(Priority.HIGH)
    public void unsecuredMethod() {
        System.out.println("Inside loggable");
    }
}
