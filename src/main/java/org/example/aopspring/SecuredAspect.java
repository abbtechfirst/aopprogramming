package org.example.aopspring;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
@Aspect
@Component
public class SecuredAspect {
    @Around("@annotation(Secured) && args(role)")
    public Object securedMethod(ProceedingJoinPoint joinPoint, Role role) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Secured secured = signature.getMethod().getAnnotation(Secured.class);
        if (secured.value() == role) {
            System.out.println("Authorized");
            return joinPoint.proceed();
        } else {
            System.out.println("Not Authorized");
            return null;
        }
    }
}